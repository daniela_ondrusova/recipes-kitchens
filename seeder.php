<?php

require_once './vendor/autoload.php';
use RedBeanPHP\R;

try {
    R::setup('mysql:host=localhost;dbname=recepten', 'bit_academy', 'bit_academy');

    // Verwijder alle bestaande keukens uit de database
    R::wipe('kitchen');

    // Lijst van keukens om te seeden
    $kitchens = [
        [
            'id' => 1,
            'name' => 'Franse keuken',
            'description' => 'De Franse keuken is een internationaal gewaardeerde keuken met een lange traditie. Deze 
                keuken wordt gekenmerkt door een zeer grote diversiteit, zoals dat ook wel gezien wordt in de Chinese 
                keuken en Indische keuken.',
        ],
        [
            'id' => 2,
            'name' => 'Chinese keuken',
            'description' => 'De Chinese keuken is de culinaire traditie van China en de Chinesen die in de diaspora 
                leven, hoofdzakelijk in Zuid-Oost-Azië. Door de grootte van China en de aanwezigheid van vele volkeren met 
                eigen culturen, door klimatologische afhankelijkheden en regionale voedselbronnen zijn de variaties groot.',
        ],
        [
            'id' => 3,
            'name' => 'Hollandse keuken',
            'description' => 'De Nederlandse keuken is met name geïnspireerd door het landbouwverleden van Nederland.
                 Alhoewel de keuken per streek kan verschillen en er regionale specialiteiten bestaan, zijn er voor 
                 Nederland typisch geachte gerechten. Nederlandse gerechten zijn vaak relatief eenvoudig en voedzaam, 
                 zoals pap, Goudse kaas, pannenkoek, snert en stamppot.',
        ],
        [
            'id' => 4,
            'name' => 'Mediterraans',
            'description' => 'De mediterrane keuken is de keuken van het Middellandse Zeegebied en bestaat onder 
                andere uit de tientallen verschillende keukens uit Marokko,Tunesie, Spanje, Italië, Albanië en Griekenland 
                en een deel van het zuiden van Frankrijk (zoals de Provençaalse keuken en de keuken van Roussillon).',
        ],
    ];

    // Loop door de keukens en voeg ze toe aan de database
    foreach ($kitchens as $kitchen) {
        $newKitchen = R::dispense('kitchen');
        $newKitchen->name = $kitchen['name'];
        $newKitchen->description = $kitchen['description'];
        R::store($newKitchen);
        echo "Keuken '{$kitchen['name']}' toegevoegd aan de database.\n";
    }

    // Verwijder alle bestaande recipes uit de database
    R::wipe('recipe');

    // Lijst van recepten om te seeden, inclusief keuken_id
    $recipes = [
        [
            'id'    => 1,
            'name'  => 'Pannekoeken',
            'type'  => 'dinner',
            'level' => 'easy',
            'kitchen_id' => 1, // Franse keuken
        ],
        [
            'id'    => 24,
            'name'  => 'Tosti',
            'type'  => 'lunch',
            'level' => 'easy',
            'kitchen_id' => 3, // Hollandse keuken
        ],
        [
            'id'    => 36,
            'name'  => 'Boeren ommelet',
            'type'  => 'lunch',
            'level' => 'easy',
            'kitchen_id' => 3, // Hollandse keuken
        ],
        [
            'id'    => 47,
            'name'  => 'Broodje Pulled Pork',
            'type'  => 'lunch',
            'level' => 'hard',
            'kitchen_id' => 4, // Mediterraans
        ],
        [
            'id'    => 5,
            'name'  => 'Hutspot met draadjesvlees',
            'type'  => 'dinner',
            'level' => 'medium',
            'kitchen_id' => 3, // Hollandse keuken
        ],
        [
            'id'    => 6,
            'name'  => 'Nasi Goreng met Babi ketjap',
            'type'  => 'dinner',
            'level' => 'hard',
            'kitchen_id' => 2, // Chinese keuken
        ],
    ];

    // Loop door de recepten en voeg ze toe aan de database
    foreach ($recipes as $recipeData) {
        $recipe = R::dispense('recipe');
        $recipe->name = $recipeData['name'];
        $recipe->type = $recipeData['type'];
        $recipe->level = $recipeData['level'];
        
        // Voeg de relatie met de keuken toe
        $kitchen = R::load('kitchen', $recipeData['kitchen_id']);
        $recipe->kitchen = $kitchen;

        R::store($recipe);
        echo "Recept '{$recipeData['name']}' toegevoegd aan de database.\n";
    }   

    R::wipe('user');

    // Voeg een gebruiker toe
    $user = R::dispense('user');
    $user->username = 'admin';
    // Hash het wachtwoord voordat het wordt opgeslagen in de database
    $hashedPassword = password_hash('admin123', PASSWORD_DEFAULT);
    $user->password = $hashedPassword;

    R::store($user);
    echo "Gebruiker 'admin' toegevoegd aan de database.\n";
} catch (PDOException $e) {
    echo $e->getmessage();
}