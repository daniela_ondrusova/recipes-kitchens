<?php

require_once '../vendor/autoload.php';

session_start();

// Controleer of de query string is meegegeven
if (isset($_GET['params'])) {
    // Ontleed de URL om de controller en methode te bepalen
    $urlParts = explode('/', $_GET['params']);
    
    // De eerste deel is de controller
    $controllerName = ucfirst($urlParts[0]) . 'Controller';

    // De tweede deel is de methode
    $method = isset($urlParts[1]) ? $urlParts[1] : 'index';

    // Controleer of het controllerbestand bestaat
    if (!class_exists($controllerName)) {
        die(error(404, "Controller bestaat niet."));
    }

    // Controleer of de methode bestaat in de controllerklasse
    if (!method_exists($controllerName, $method)) {
        die(error(404, 'Methode bestaat niet in de controller.'));
    }
} else {
    // Geen URL meegegeven, gebruik standaard controller en methode
    $controllerName = 'RecipeController';
    $method = 'index';
}

// Instantiëer de controller
$controller = new $controllerName();

// Roep de methode aan op de controller op basis van de HTTP-methode

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    // Roep de GET-methode aan op de controller
    if (method_exists($controller, $method)) {
        $controller->$method();
    } else {
        die(error(404, 'Methode niet gevonden!'));
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Roep de POST-methode aan op de controller, bijvoorbeeld handleCreateRecipe
    $postMethod = $method . 'Post';
    if (method_exists($controller, $postMethod)) {
        $controller->$postMethod();
    } else {
        die(error(404, 'POST-methode niet gevonden!'));
    }
} 

