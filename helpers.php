<?php

require_once 'vendor/autoload.php';

function displayTemplate($template, $variables = [])
{
    $loader = new \Twig\Loader\FilesystemLoader('../views');
    $twig = new \Twig\Environment($loader);
    $twig->addGlobal('session', $_SESSION);
    return $twig->display($template, $variables);
}

function error($errorNumber, $errorMessage) 
{
     http_response_code($errorNumber);
     displayTemplate('error.twig', ['errorMessage' => $errorMessage]);
     exit;
}