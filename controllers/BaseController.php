<?php

use RedBeanPHP\R;

class BaseController
{
    public function __construct()
    {
        // Setup database connection
        R::setup('mysql:host=localhost;dbname=recepten', 'bit_academy', 'bit_academy');
    }

    public function getBeanById($typeOfBean, $queryStringKey) 
    {    
        // Controleer of de ID is opgegeven in de query string
        if (isset($_GET[$queryStringKey])) {
            // Haal de ID op uit de query string
            $id = $_GET[$queryStringKey];
            
            // Zoek de bean op basis van het type en de ID
            $bean = R::findOne($typeOfBean, 'id = ?', [$id]);
            
            // Return de gevonden bean
            return $bean;
        } else {
            // Als er geen ID is opgegeven, return null
            return null;
        }
    }

    protected function authorizeUser()
    {
        // Controleer of de gebruikers-ID is ingesteld in de sessie
        if (!isset($_SESSION['user_id'])) {
            // Gebruiker is niet ingelogd, redirect naar de loginpagina
            header("Location: /user/login");
            exit();
        }
    }
}