<?php 

use RedBeanPHP\R;

include "BaseController.php";

class KitchenController extends BaseController
{
    public function create()
    {
        $this->authorizeUser();

        // Geef de constanten door aan de view
        displayTemplate('kitchens/create.twig');
    }

    public function createPost() 
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['name']) && isset($_POST['description'])) {
                // Haal de ingediende gegevens op uit het formulier
                $name = $_POST['name'];
                $description = $_POST['description'];
    
                // Maak een nieuw receptobject aan met RedBean
                $kitchen = R::dispense('kitchen');
                $kitchen->name = $name;
                $kitchen->description = $description;
    
                // Sla het nieuwe recept op in de database
                $id = R::store($kitchen);
    
                // Redirect naar de show methode van het zojuist aangemaakte recept
                header("Location: show?id=$id");
                exit;
            } else {
                // Niet alle vereiste velden zijn ingevuld, toon een foutmelding of redirect terug naar het formulier
                die("Niet alle vereiste velden zijn ingevuld.");
            }
        } else {
            // Verzoeksmethode is geen POST, toon een foutmelding of redirect terug naar het formulier
            die("Dit endpoint ondersteunt alleen POST-verzoeken.");
        }
    }

    public function edit() 
    {
        $this->authorizeUser();

        // Haal het recept op uit de database
        $kitchen = R::load('kitchen', $_GET['id']);

        // Controleer of het recept bestaat
        if (!$kitchen->id) {
            // Toon een foutmelding als het recept niet gevonden is
            error(404, "No kitchen found with the specified ID!");
        }

        // Geef de view weer om het recept te bewerken, samen met de gegevens van het recept
        displayTemplate('kitchens/edit.twig', ['kitchen' => $kitchen]);
    }

    public function editPost()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['description'])) {
                // Haal het ID van de keuken op uit de POST-data
                $id = $_POST['id'];

                // Haal de keuken op uit de database
                $kitchen = R::load('kitchen', $id);

                // Controleer of de keuken bestaat
                if (!$kitchen->id) {
                    // Toon een foutmelding als de keuken niet gevonden is
                    error(404, "No kitchen found with the specified ID!");
                }

                // Werk de eigenschappen van de keuken bij met de waarden uit het formulier
                $kitchen->name = $_POST['name'];
                $kitchen->description = $_POST['description'];

                // Sla de wijzigingen op in de database
                R::store($kitchen);

                // Redirect naar de show methode van de bewerkte keuken
                header("Location: /kitchen/show?id=$id");
                exit;
            } else {
                // Niet alle vereiste velden zijn ingevuld, toon een foutmelding of redirect terug naar het formulier
                die("Niet alle vereiste velden zijn ingevuld.");
            }
        } 
    }

    public function index()
    {
        // Recepten ophalen uit de database
        $kitchens = R::findAll('kitchen');

        // Display de indexpagina met de lijst van gerechten
        displayTemplate('kitchens/index.twig', ['kitchens' => $kitchens]);
    }

    public function show()
    {
        // Gebruik de getBeanById methode om de keuken te vinden
        $kitchen = $this->getBeanById('kitchen', 'id');

        if (!$kitchen) {
            // Als de keuken niet gevonden is, toon een foutmelding
            error(404, "No kitchen found with the specified ID!");
        }

        // Haal alle recepten op die bij de keuken horen
        $recipes = R::find('recipe', 'kitchen_id = ?', [$kitchen->id]);

        // Toon de keukendetails
        displayTemplate('kitchens/show.twig', [
            'kitchen' => $kitchen,
            'recipes' => $recipes
        ]);
    }
}