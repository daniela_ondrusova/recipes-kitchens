<?php

use RedBeanPHP\R;

include "BaseController.php";

class UserController extends BaseController
{
    public function login()
    {
        // Controleer of er een foutmelding is ingesteld in de sessie
        $error = isset($_SESSION['error']) ? $_SESSION['error'] : null;
        // Verwijder de foutmelding uit de sessie
        unset($_SESSION['error']);

        displayTemplate('users/login.twig', ['error' => $error]);
    }
    
    public function loginPost()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['username']) && isset($_POST['password'])) {
                $username = $_POST['username'];
                $password = $_POST['password'];
    
                // Zoek de gebruiker op basis van de ingevoerde gebruikersnaam
                $user = R::findOne('user', 'username = ?', [$username]);
    
                // Controleer of de gebruiker is gevonden en het wachtwoord overeenkomt
                if ($user && password_verify($password, $user->password)) {
                    // Gebruiker succesvol ingelogd, sla het gebruikers-ID op in de sessie
                    $_SESSION['user_id'] = $user->id;
                    $_SESSION['user'] = $username;

                    // Redirect naar de receptenlijst
                    header("Location: /recipe");
                    exit();
                } else {
                    // Gebruikersnaam of wachtwoord is onjuist, sla een foutmelding op in de sessie
                    $_SESSION['error'] = "Ongeldige gebruikersnaam of wachtwoord.";
                    // Redirect terug naar de inlogpagina
                    header("Location: /user/login");
                    exit();
                }
            } else {
                // Niet alle vereiste velden zijn ingevuld, sla een foutmelding op in de sessie
                $_SESSION['error'] = "Ongeldige gebruikersnaam of wachtwoord.";
                // Redirect terug naar de inlogpagina
                header("Location: /user/login");
                exit();
            }
        } 
    }
    
    public function logout()
    {
        // Start de sessie
        session_start();

        // Vernietig alle sessievariabelen
        session_unset();

        // Vernietig de sessie zelf
        session_destroy();

        // Redirect naar de inlogpagina
        header("Location: /user/login");
        exit();
    }

    public function register() 
    {
        // Controleer of er een foutmelding is ingesteld in de sessie
        $error = isset($_SESSION['error']) ? $_SESSION['error'] : null;
        // Verwijder de foutmelding uit de sessie
        unset($_SESSION['error']);

        displayTemplate('users/register.twig', ['error' => $error]);
    }

    public function registerPost()
    {
        // Controleer of de registratieformulier is ingediend
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['confirm_password'])) {
                $username = $_POST['username'];
                $password = $_POST['password'];
                $confirmPassword = $_POST['confirm_password'];

                // Controleer of de wachtwoorden overeenkomen
                if ($password !== $confirmPassword) {
                    // Passwords komen niet overeen, toon een foutmelding
                    $_SESSION['error'] = 'Passwords did not match.';
                    header("Location: /user/register");
                    exit();
                }

                // Controleer of de gebruikersnaam al bestaat
                if (R::findOne('user', 'username = ?', [$username])) {
                    // Gebruikersnaam is al in gebruik, toon een foutmelding
                    $_SESSION['error'] = 'Username is already in use.';
                    header("Location: /user/register");
                    exit();
                }

                // Gebruiker bestaat nog niet, maak een nieuwe gebruiker aan
                $user = R::dispense('user');
                $user->username = $username;
                // Hash het wachtwoord voordat het wordt opgeslagen in de database
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                $user->password = $hashedPassword;

                // Sla de gebruiker op in de database
                R::store($user);

                // Gebruiker succesvol geregistreerd, log de gebruiker meteen in
                $_SESSION['user_id'] = $user->id;

                // Redirect naar een gepaste pagina, bijvoorbeeld de homepagina
                header("Location: /recipe");
                exit();
            } else {
                // Niet alle vereiste velden zijn ingevuld, toon een foutmelding
                $_SESSION['error'] = 'All fields are required.';
                header("Location: /user/register");
                exit();
            }
        } else {
            // Toon het registratieformulier
            displayTemplate('users/register.twig');
        }
    }
}