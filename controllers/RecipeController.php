<?php

use RedBeanPHP\R;

include "BaseController.php";

class RecipeController extends BaseController
{
    // Definieer constanten voor moeilijkheid en type
    public const LEVEL = ['easy', 'medium', 'hard'];
    public const TYPE = ['breakfast', 'lunch', 'dinner', 'dessert'];

    public function create()
    {
        $this->authorizeUser();

        $kitchens = R::findAll('kitchen');

        // Geef de constanten door aan de view
        displayTemplate('recipes/create.twig', [
            'kitchens' => $kitchens,
            'levels' => self::LEVEL,
            'types' => self::TYPE
        ]);
    }

    public function createPost() 
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['name']) && isset($_POST['level']) && isset($_POST['type'])) {
                $kitchenId = $_POST['kitchen_id'];

                // Haal de ingediende gegevens op uit het formulier
                $name = $_POST['name'];
                $level = $_POST['level'];
                $type = $_POST['type'];
    
                // Maak een nieuw receptobject aan met RedBean
                $recipe = R::dispense('recipe');
                $recipe->name = $name;
                $recipe->level = $level;
                $recipe->type = $type;
    
                // Haal de ID op van de geselecteerde keuken en stel deze in als kitchen_id
                $recipe->kitchen_id = $kitchenId;

                // Sla het nieuwe recept op in de database
                $id = R::store($recipe);
    
                // Redirect naar de show methode van het zojuist aangemaakte recept
                header("Location: /kitchen/show&id=$kitchenId");
                exit;
            } else {
                // Niet alle vereiste velden zijn ingevuld, toon een foutmelding of redirect terug naar het formulier
                die("Niet alle vereiste velden zijn ingevuld.");
            }
        } else {
            // Verzoeksmethode is geen POST, toon een foutmelding of redirect terug naar het formulier
            die("Dit endpoint ondersteunt alleen POST-verzoeken.");
        }
    }

    public function edit() 
    {
        $this->authorizeUser();

        // Haal het recept op uit de database
        $recipe = R::load('recipe', $_GET['id']);

        // Controleer of het recept bestaat
        if (!$recipe->id) {
            // Toon een foutmelding als het recept niet gevonden is
            error(404, "No recipe found with the specified ID!");
        }

        $kitchens = R::findAll('kitchen');

        // Geef de view weer om het recept te bewerken, samen met de gegevens van het recept
        displayTemplate('recipes/edit.twig', [
            'recipe' => $recipe,
            'kitchens' => $kitchens,
            'levels' => self::LEVEL,
            'types' => self::TYPE
        ]);
    }

    public function editPost()
    {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            // Controleer of alle vereiste velden zijn ingevuld
            if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['level']) && isset($_POST['type'])) {
                // Haal het ID van het recept op uit de POST-data
                $id = $_POST['id'];
                $kitchenId = $_POST['kitchen_id'];

                // Haal het recept op uit de database
                $recipe = R::load('recipe', $id);

                // Controleer of het recept bestaat
                if (!$recipe->id) {
                    // Toon een foutmelding als het recept niet gevonden is
                    error(404, "No recipe found with the specified ID!");
                }

                // Werk de eigenschappen van het recept bij met de waarden uit het formulier
                $recipe->name = $_POST['name'];
                $recipe->level = $_POST['level'];
                $recipe->type = $_POST['type'];

                $recipe->kitchen_id = $kitchenId;

                // Sla de wijzigingen op in de database
                R::store($recipe);

                // Redirect naar de show methode van het bewerkte recept
                header("Location: /kitchen/show?id=$kitchenId");
                exit;
            } else {
                // Niet alle vereiste velden zijn ingevuld, toon een foutmelding of redirect terug naar het formulier
                die("Niet alle vereiste velden zijn ingevuld.");
            }
        } 
    }

    public function index()
    {       
        // Recepten ophalen uit de database
        $recipes = R::findAll('recipe');

        // Display de indexpagina met de lijst van gerechten
        displayTemplate('recipes/index.twig', ['recipes' => $recipes]);
    }

    public function show()
    {
        $recipe = $this->getBeanById('recipe', 'id');
        if (!$recipe) {
            // Als het recept niet gevonden is, toon een foutmelding
            error(404, "No recipe found with the specified ID!");
        }

        // Haal de keuken op waartoe het recept behoort
        $kitchen = $recipe->kitchen;

        // Toon de receptdetails met de bijbehorende keuken
        displayTemplate('recipes/show.twig', ['recipe' => $recipe, 'kitchen' => $kitchen]);
    }
}