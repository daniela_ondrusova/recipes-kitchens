### Installatie

Installeer Twig via Composer in de project-root:

```bash
composer require "twig/twig:^3.0"
```

Of 
```bash
composer install
```

Als je naar <http://localhost> gaat, krijg je de tekst uit de template te zien.  
